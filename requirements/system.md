# Libro system

| Capítulo | Contenido|
|------|------|
| Componentes | 1. Microbits para auxiliares y pacientes. Cada uno con un identificador único |
|| 2. Microbit instalados en las salas y conectados a un ordenador para la identificaciones de cada sala|
|| 3. Ordenador central con la BBDD |
| Funcionalidad | 1.Envio de alarmas por parte del paciente/anciano.|
|| 1.1 Alarma automática: Cuando en acelerómetro detecta una caída (por un cambio de velocidad muy rápido), se manda una alarma por broadcast a todos los auxiliares con el identificador del anciano afectado y con el identificador de la sala donde se ha producido la caída.|
|| 1.2 Alarma manual: Cuando el anciano se encuentra en algún problema pulsa botón programado para enviar una alarma por broadcast a todos los auxiliares con el identificador del anciano y el de la sala donde se encuentra|
|| 2. Atención de los auxiliares a los ancianos. En todos los casos cuando a los auxiliares les llega una alarma tiene que pulsar un botón del microbit para aceptar dicha alarma y cuando ya está atendiendo al anciano pulsar el botón del microbit del anciano|
|| 3. Recordatorio de horarios en medicamentos personalizados para cada paciente. En una BBDD está guardada toda la medicación y el horario al que tiene que tomarla cada paciente. Se envía por broadcast el identificador del anciano, identificador de la medicación y el identificador de la sala en la que se encuentra el paciente|
|| 3. Obtención de información sobre la sala en la que se encuentran. Cada minuto se guarda en una BBDD el identificador de anciano con el identificador de la sala donde se encuentra|
|| 4. Alarma en caso de abandono del centro sin autorización. Cuando no se recibe potencia de un microbit o se recibe la mínima se crea una alarma con el identificador del anciano al que pertenece dicho microbit. Se manda por broadcast a todos los auxiliares y si está muy lejos de algún ordenador se utilizan el resto de microbits como repetidores para hacer que la alarma llegue de manera satisfactoria|
| Interfaces | Se utilizará API Rest (get,put y post) para interactuar la BBDD|
| Escenarios de usos detallados | 1. Caida de un anciano: el microbit mediante un acelerómetro calcula la velocidad de movimiento en 3 ejes y actúa enviando una señal de alarma a los distintos auxiliares |
|| 2. Anciano pulsa el botón porque se encuentra mal: el microbit dispone de un botón con el que se podrá enviar una señal de alerta a los auxiliares (manual) si la situación lo requiere.|
|| 3. Historial de localización de los ancianos: cada 15 se envía una señal a las distintas salas para confirmar la posición en la que se encuentra y así poder recoger la información en una base de datos con la fecha, hora, sala e identificador del microbit de cada anciano.|
|| 4. Aviso en caso de que el anciano se haya salido del recinto: en el momento que la sala no reciba la potencia del microbit enviará una alarma para indicar que no lo detecta y puede ser debido a una averia o a que no se encuentra en el recinto. |
|| 5. Alarma en microbit enfermer@ para la medicación: el microbit envia una alarma a auxiliares a cierta hora para el recordatorio de los medicamentos de los ancianos que tienen asignados. |
|Priorización  |  1. Caida del anciano |
|| 2. Abandono del centro|
|| 3. Pulsa botón de urgencia |
| Verificación y criterio de aceptación | 1. Caida de un anciano: señal de alarma rápida y efectiva a los auxiliares |
|| 2. Anciano tiene urgencia: botón fácilmente accesible y señal de alarma rápida y efectiva a los auxiliares |
|| 3. Historial: comprobación del buen registro de información en la base de datos |
|| 4. Abandono del centro: señal de alarma rápida y efectiva a los auxiliares |
|| 5. Horario medicación: señal de recordatorio sincronizada con la hora para la toma de dicha medicación |

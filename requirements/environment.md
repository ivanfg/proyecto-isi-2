
|Libro de entorno |               |
|-----------|-------------------|
| **Glosario**  | - Microbit: Dispositivo programable que se adjudica a cada anciano y auxiliar para gestionar el sistema.| 
||- LEDs: Peque�a bombilla de color que se activa seg�n este programado.| 
||- Botones programables: Botones programados que realizan funciones espec�ficas|
||- Pines: Sirven de conectores|
||- Sensor t�ctil: Sensor que reconoce cualquier tacto|
||- Aceler�metro: Se activa cada vez que detecte una aceleraci�n|
||- Magnet�metro: Instrumento para medir la fuerza y la direcci�n de un campo magn�tico|
||- Sensor de temperatura: Sensor que detecta la temperatura| 
||- Radio Blueetooth: Permite la comunicaci�n rf de corto alcance entre un equipo y un dispositivo de entrada, un dispositivo de audio u otro perif�rico de usuario conectado a Bluetooth.       |
| **Componentes**  |Ordenador Central, BBDD, Microbit, Ancianos, Auxiliares|
| **Restricciones** |- Cada anciano debe llevar el microbit fijo a la ropa|
||- Un per�metro de la residencia fijo|                    
| **Supuestos**  |- Microbit de cada anciano que sirve de repetidor|
||- Conocimiento m�nimo sobre el uso de las funciones del microbit|
||- Comportamiento adecuado de los auxiliares con el microbit|
| **Efectos** |Ordenador Central, BBDD, Microbit|
| **Invariantes** | Reciclado de los microbits |

# Libro Goals

| Capítulo | Contenido|
|------|------|
| Contexto y objetivo general | Proyecto creado para ayudar y mejorar la calidad de vida de ancianos y auxiliares en residencias. También podría implantarse en guarderías y hospitales. Proyecto basado en el seguimiento de pacientes u ancianos para un mayor control que garantice su bienestar.
| Situación actual | En vías de desarrollo |
| Beneficios esperados | Con el desarrollo de este proyecto se espera cambiar la gestión de centros tales como los indicados para ofrecer una mayor garantía de seguridad y calidad en la vida de pacientes y auxiliares.
| Descripción general de la funcionalidad | Funciones: |
 || 1.Envio de alarmas por parte del paciente/anciano, ya sea por caidas (automático) o por urgencia (manual).|
 || 2. Recordatorio de horarios en medicamentos personalizados para cada paciente.|
 || 3. Obtención de información sobre la sala en la que se encuentran.|
 || 4. Alarma en caso de abandono del centro sin autorización|
 Escenarios de uso de alto nivel | 1. Si un anciano sufre una caida se envia una alarma a todos los auxiliares para que le ayuden de forma efectiva|
 || 2. Si un anciano sufre indisponibilidad o urgencia puede activar con el botón una alarma que llegará a los auxiliares |
 || 3. Alarma automática personalizada para el auxiliar a la hora de la toma de medicamentos por parte del anciano |
 || 4. Al abandonar el centro sin autorización se mandará una alarma al centro evitando así males mayores |
 Limitaciones y exclusiones |  Este sistema no podrá recoger información sobre signos vitales, temperatura corporal, nivel de azúcar en sangre, etc. |
 || Asuntos de interés y fuentes de requisitos| 1. Auxiliares médicos. |
 || 2. Pacientes (ancianos, niños, personas hospitalizadas...) |

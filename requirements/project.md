
|Libro de proyecto |               |
|-----------|-------------------|
| **Funciones y personal** |- Daniel �lvarez Bustos|
||- Iv�n Fern�ndez Garc�a|
||- Rodrigo Garc�a Diez|
||- Paula Pi�eiro Aliaga|
||- Pablo Cabeza Portalo|
||- Otros desarrolladores|
| **Elecciones t�cnicas impuestas** |- Python|
||- Mu-editor|
||- Castellano|
| **Cronograma e hitos** | Por decidir |
| **Tareas y entregables**  | Por decidir  |
| **Elementos tecnol�gicos requeridos** |- Microbits|
||- Ordenador central|
||- BBDD|
||- Ordenador por sala|
| **An�lisis de riesgos y mitigaci�n** | Por decidir  |
| **Proceso e informe de requerimientos** |- Microbits para la ayuda y localizaci�n de los ancianos|
||- BBDD para guardar el historial de cada anciano|
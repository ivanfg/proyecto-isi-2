# **_Primera historia de usuario_**
#### **Caida de un anciano**
- **Como un** auxiliar
- **Para que** pueda ayudar al anciano
- **Quiero** recibir una alarma con la ubicación del anciano cuando el microbit detecte la caida

# **_Segunda historia de usuario_**
#### **Cuando el anciano pulsa el botón porque se encuentra mal**
- **Como un** auxiliar
- **Para que** pueda ayudar al anciano
- **Quiero** recibir una alarma con la ubicación del anciano cuando pulse el botón

# **_Tercera historia de usuario_**
#### **Historial de localización de los ancianos**
- **Como un** encargado
- **Para que** pueda ayudar al anciano
- **Quiero** que se guarde el historial de localización

# **_Cuarta historia de usuario_**
#### **Aviso en caso de que el anciano se haya salido del recinto**
- **Como un** auxiliar
- **Para que** pueda ayudar y encontrar al anciano
- **Quiero** recibir una alarma con la última ubicación del anciano cuando no se detecte la señal del microbit.

# **_Quinta historia de usuario_**
#### **Alarma en microbit enfermer@ para la medicación**
- **Como un** auxiliar
- **Para que** pueda medicar al anciano
- **Quiero** recibir una alarma con el aviso de la hora de la medicación,la ubicación y el nombre del anciano para saber que medicamentos darle.

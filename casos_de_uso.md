# **Primer caso**
| **Casos de uso** |               |
|-----------|-------------------|
| **Nombre** | Caida de un anciano            | 
| **Alcance** | Ordenador central de la residencia  |
| **Nivel** |    Alto   |
| **Actor principal**  | Auxiliar de enfermería |
| **Contexto de uso**  | El auxiliar de enfermería maneja el microbit cuando salta la alarma|
| **Condiciones previas**     |   El anciano ha sufrido una caida |
| **Generar** |  Se informa a los auxiliares de enfermeria y se registra en la BBDD que ha habido una caida.|
| **Principal exito guión** | 1.El microbit informa a los auxiliares de la caida del anciano y se registra en la BBDD |
||2.Un auxiliar recibe la señal y la acepta |
||3.El auxiliar acepta la señal:|
||3.1 Acude a la sala donde se encuentra el anciano|
||3.2 Cuando ya se encuentra con el anciano desactiva la alarma pulsando el botón del microbit del anciano|
| **Garantia de exito** |  El anciano esta atendido |
| **Extensiones** | A. Dos auxiliares aceptan la señal a la vez y el microbit elige al auxiliar que mas cerca este |
| **Partes interesadas y intereses** | 1. Auxiliares|
||2. Ancianos|
||3. Empresa microbit|

# **Segundo caso**
| **Casos de uso** |               |
|-----------|-------------------|
| **Nombre** | Cuando el anciano pulsa el botón porque se encuentra mal o tiene un problema|
| **Alcance** | Ordenador central de la residencia  |          
| **Nivel** |    Alto   |
| **Actor principal**  | Auxiliar de enfermería |
| **Contexto de uso**  | El anciano pulsa el botón y el auxiliar de enfermería maneja el microbit cuando salta la alarma|
| **Condiciones previas**  | El anciano se encuentra mal o en problemas |
| **Generar** |  Se informa a los auxiliares de enfermeria y se registra en la BBDD que ha habido un problema|
| **Principal exito guión** | 1.El microbit informa a los auxiliares y se registra en la BBDD | 
||2.Un auxiliar recibe la señal y la acepta |
||3.El auxiliar acepta la señal:|
||3.1 Acude a la sala donde se encuentra el anciano|
||3.2 Cuando ya se encuentra con el anciano desactiva la alarma pulsando el botón del microbit del anciano|
| **Garantia de exito** |  El anciano esta atendido |
| **Extensiones** | A. Dos auxiliares aceptan la señal a la vez y el microbit elige al auxiliar que mas cerca este |
| **Partes interesadas y intereses** | 1. Auxiliares|
||2. Ancianos|
||3. Empresa microbit|

# **Tercer caso**
| **Casos de uso** |               |
|-----------|-------------------|
| **Nombre** | Historial de localización de los ancianos|
| **Alcance** |  Ordenador central de la residencia |   
| **Nivel** |    Alto   |
| **Actor principal**  | Auxiliar de enfermería y la empresa de microbit |
| **Contexto de uso**  | Se guarda en la BBDD la posición de cada anciano cada 15 minutos|
| **Condiciones previas** | Control de la ubicación de cada anciano por si ocurre algún problema |
| **Generar** | Se genera un historial para ser registrado en la BBDD|
| **Principal exito guión** | 1.Se guarda el historial de localización para poder usarlo en caso de que haya algún problema o incidencia |
| **Garantia de exito** |  Control sobre el anciano para garantizar su seguridad |
| **Extensiones** | A. Asegurarnos de que se registra correctamente la ubicación de cada anciano |
| **Partes interesadas y intereses** | 1. Auxiliares|
||2. Ancianos|
||3. Empresa microbit|

# **Cuarto caso**
| **Casos de uso** |               |
|-----------|-------------------|
| **Nombre** | Aviso en caso de que el anciano se haya salido del recinto|
| **Alcance** |  Ordenador central de la residencia|   
| **Nivel** |    Alto   |
| **Actor principal**  | Auxiliar de enfermería|
| **Contexto de uso**  | El ordenador central hace llegar la señal de alarma a los microbit de cada auxilizar|
| **Condiciones previas**  | Control de la ubicación de cada anciano en caso de que se aleje |
| **Generar** |  Se informa a los auxiliares de enfermeria y nos llega una alarma|
| **Principal exito guión** | 1.El microbit informa cada minuto la posición donde se encuentra(Sala). Si se deja de recibir la señal se informa a los auxiliares y se registra en la BBDD que ha habido un problema. |
||2.Un auxiliar recibe la señal y la acepta |
||3.El auxiliar acepta la señal:|
||3.1 Acude a la ultima ubicación del anciano|
||3.2 Sale en busca del anciano para ayudarle|
||3.3 El auxiliar pulsa el botón cuando llegue a la residencia para garantizar que el anciano ya ha sido encontrado| 
| **Garantia de exito** | Control sobre el anciano para garantizar su seguridad |
| **Extensiones** |  A. Dos auxiliares aceptan la señal a la vez y el microbit elige al auxiliar que mas cerca este |
| **Partes interesadas y intereses** | 1. Auxiliares|
||2. Ancianos|
||3. Empresa microbit|

# **Quinto caso**
| **Casos de uso** |               |
|-----------|-------------------|
| **Nombre** |  Alarma en microbit enfermer@ para la medicación|
| **Alcance** |  Ordenador central de la residencia |   
| **Nivel** |    Alto   |
| **Actor principal**  | Auxiliar de enfermería|
| **Contexto de uso**  | El ordenador central hace llegar la señal de alarma a los microbit de cada auxilizar|
| **Condiciones previas**  | Control de la medicación de cada anciano |
| **Generar** |  Se informa a los auxiliares de enfermeria|
| **Principal exito guión** | 1.El microbit informa a los auxiliares con nombre,hora y ubicación| 
||2.Un auxiliar recibe la señal y la acepta |
||3.El auxiliar acepta la señal:|
||3.1 Acude a la sala donde se encuetra el anciano|
||3.2 Procede a darle los medicamentos|
||3.3 El auxiliar pulsa el botón confirmando de que el anciano ya ha sido atendido| 
| **Garantia de exito** |  Control sobre el anciano para garantizar su seguridad |
| **Extensiones** |  A. Dos auxiliares aceptan la señal a la vez y el microbit elige al auxiliar que mas cerca este |
| **Partes interesadas y intereses** | 1. Auxiliares|
||2. Ancianos|
||3. Empresa microbit|